# Prayer Calendar

Prayer times for busy folks!&trade;

## Credits

- cities.tsv is a trimmed and rearranged version of http://download.geonames.org/export/dump/cities15000.zip
- countries.tsv is a trimmed version of http://download.geonames.org/export/dump/countryInfo.txt
