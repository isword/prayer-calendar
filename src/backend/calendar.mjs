import {ICalCalendar} from 'ical-generator';
import fs from 'fs';

function saveCalendar(name, filename, events) {
    const calendar = new ICalCalendar({
        name: name,
        prodId: {
            company: 'iSWORD.nl',
            product: 'PrayerCalendar',
            language: 'EN',
        },
    });

    events.forEach(event => calendar.createEvent(event));

    fs.writeFileSync(`${filename}.ics`, calendar.toString());
}

export {saveCalendar};
