import {getPrayerTimes, methodAndCityMatch, CalculationMethod, Madhab} from "./prayer-times.mjs";
import {streamRows, getRowCount} from "./tsv.mjs";
import {DateTime} from 'luxon';
import {saveCalendar} from "./calendar.mjs";
import {mapPrayerTimesToEvents} from "./prayer-times-converter.mjs";

const appName = process.env.npm_package_name;
const today = DateTime.local(); // Get current date and time
const endDate = today.plus({ months: 2 }); // Add 2 months to current date

const citiesTsvPath = 'src/assets/cities.tsv';
const outPath = 'calendars';
const isDippingToes = process.argv.includes('--dip-toes');
const isDryRun = process.argv.includes('--dry-run');

let citiesProcessed = 0;

let totalCalendarsCreated = 0;
let totalEventsCreated = 0;
let totalErrorsEncountered = 0;

const citiesCount = getRowCount(citiesTsvPath);

console.time(appName);

for await (const city of streamRows(citiesTsvPath)) {
    let calendarsCreated = 0;
    let eventsCreated = 0;
    let errorsEncountered = 0;

    const calendarName = `${city.name} Prayer Times`;

    !isDryRun && console.time(city.name);

    for (let calculationMethodKey in CalculationMethod) {
        // Do not create calendars for region-specific methods outside their regions
        if (!methodAndCityMatch(calculationMethodKey, city) || calculationMethodKey === 'Other') {
            continue;
        }

        for (let madhabKey in Madhab) {
            let prayerTimes = [];

            const calendarFilename = [
                city.id,
                calculationMethodKey,
                madhabKey
            ].join('-');

            for (let day = today; day <= endDate; day = day.plus({ days: 1 })) {
                const dayPrayerTimes = getPrayerTimes(
                    city.latitude, city.longitude,
                    day.year, day.month, day.day,
                    CalculationMethod[calculationMethodKey](),
                    Madhab[madhabKey], // use recommended high latitudes method
                );

                // Sometimes things can go south, and that's okay
                if (Object.values(dayPrayerTimes).some(date => isNaN(date))) {
                    errorsEncountered++;
                    continue;
                }

                prayerTimes.push(dayPrayerTimes);
            }

            const events = mapPrayerTimesToEvents(prayerTimes);

            !isDryRun && saveCalendar(calendarName, `${outPath}/${calendarFilename}`, events);

            calendarsCreated++;
            eventsCreated += events.length;
        }
    }

    !isDryRun && console.timeEnd(city.name);

    citiesProcessed++;

    if (errorsEncountered > 0) {
        console.error(`${errorsEncountered} errors encountered!`)
    }

    if (!isDryRun || citiesProcessed % 100 === 0) {
        console.log(`Created ${calendarsCreated} calendars including ${eventsCreated} events - (${(citiesProcessed / citiesCount * 100).toFixed(2)}%)\n`);
    }

    totalCalendarsCreated += calendarsCreated;
    totalEventsCreated += eventsCreated;
    totalErrorsEncountered += errorsEncountered;

    if (isDippingToes && citiesProcessed >= 10) {
        break;
    }
}

console.timeEnd(appName);
console.log(`Created a total of ${totalCalendarsCreated} calendars including ${totalEventsCreated} events`);

if (totalErrorsEncountered > 0) {
    console.error(`A total of ${totalErrorsEncountered} error(s) encountered!`)
}
