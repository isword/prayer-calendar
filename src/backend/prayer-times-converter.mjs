import {DateTime} from 'luxon';

function mapPrayerTimesToEvents(prayerTimes) {
    return prayerTimes.flatMap((prayerTimes) => (
        Object.entries(prayerTimes).map(([name, time]) => ({
            summary: name,
            // timezone: name,
            start: time,
            // Give Jummah prayer some more time (don't skip your Jummah prayer!)
            end: DateTime.fromJSDate(time).plus({minutes: name === 'Jummah' ? 45 : 15}).toJSDate(),
        }))
    ));
}

export {mapPrayerTimesToEvents};
