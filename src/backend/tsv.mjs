import fs from 'fs';
import csv from 'csv-parser';
import {execSync} from 'child_process';

async function* streamRows(filePath) {
    const stream = fs.createReadStream(filePath, { encoding: 'utf-8' });

    for await (const row of stream.pipe(csv({
        separator: '\t', // Use tabs as separators
        mapHeaders: ({ header }) => header.trim().toLowerCase() // Trim and lowercase headers
    }))) {
        yield row;
    }
}

async function readAll(filePath) {
    const rows = [];
    return new Promise((resolve, reject) => {
        fs.createReadStream(filePath, { encoding: 'utf-8' })
            .pipe(csv({
                separator: '\t', // Use tabs as separators
                mapHeaders: ({ header }) => header.trim().toLowerCase() // Trim and lowercase headers
            }))
            .on('data', (data) => rows.push(data))
            .on('end', () => resolve(rows))
            .on('error', (error) => reject(error));
    });
}

async function addRow(filePath, row) {
    // Check if the file exists
    const fileExists = fs.existsSync(filePath);
    const fileSize = fs.statSync(filePath).size;
    if (!fileExists || fileSize === 0) {
        // If the file does not exist, create it with headers
        await fs.promises.writeFile(filePath, Object.keys(row).join('\t') + '\n');
    }

    // Convert the row data to a comma-separated string
    const rowString = Object.values(row).join('\t');

    // Append the row to the end of the file
    await fs.promises.appendFile(filePath, rowString + '\n');
}

function getRowCount(filePath) {
    return parseInt(execSync(`wc -l ${filePath}`));
}

export {streamRows, readAll, addRow, getRowCount};
