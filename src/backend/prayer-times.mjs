import {
  Coordinates,
  CalculationMethod,
  PrayerTimes,
  Madhab,
  HighLatitudeRule,
} from 'adhan';

function getPrayerTimes(latitude, longitude, year, month, day, method, madhab = null, highLatitudeRule = null) {
  const coordinates = new Coordinates(latitude, longitude);
  const params = method;

  if (madhab != null) {
    params.madhab = madhab;
  }

  if (highLatitudeRule != null) {
    params.highLatitudeRule = highLatitudeRule;
  }

  const date = new Date(year, month - 1, day);

  const result = new PrayerTimes(coordinates, date, params);

  const isFriday = date.getDay() === 5; // 0 = Sunday, and so on..

  return {
    Fajr: result.fajr,
    Sunrise: result.sunrise,
    [isFriday ? 'Jummah' : 'Dhuhr']: result.dhuhr,
    Asr: result.asr,
    Sunset: result.sunset,
    // Maghrib: result.maghrib,
    Isha: result.isha,
  };
}

function methodAndCityMatch(methodName, city) {
  if (isGenerallyAcceptedMethod(methodName)) {
    return true;
  }

  // Could've also checked city.country !== 'AE'
  if (methodName === 'Dubai' && city.timezone !== 'Asia/Dubai') {
    return false;
  }

  if (methodName === 'Qatar' && city.timezone !== 'Asia/Qatar') {
    return false;
  }

  if (methodName === 'Kuwait' && city.timezone !== 'Asia/Kuwait') {
    return false;
  }

  if (methodName === 'Singapore' && !['SG', 'MY', 'ID'].includes(city.country)) {
    return false;
  }

  if (methodName === 'Turkey' && city.country !== 'TR') {
    return false;
  }

  if (methodName === 'Tehran' && city.country !== 'IR') {
    return false;
  }

  if (methodName === 'NorthAmerica' && !['CA', 'US'].includes(city.country)) {
    return false;
  }

  if (methodName === 'MoonsightingCommittee' && !['CA', 'US', 'GB'].includes(city.country)) {
    return false;
  }

  return true;
}

function getRecommendedMethod(city) {
  if (city.country === 'AE') {
    return 'Dubai';
  }

  if (city.country === 'QA') {
    return 'Qatar';
  }

  if (city.country === 'KW') {
    return 'Kuwait';
  }

  if (city.country === 'TR') {
    return 'Turkey';
  }

  if (city.country === 'IR') {
    return 'Tehran';
  }

  if (['DZ', 'EG', 'LY', 'NG', 'SS', 'SD'].includes(city.country)) {
    return 'Egyptian';
  }

  if (['CA', 'US', 'GB'].includes(city.country)) {
    return 'MoonsightingCommittee'; // Why not NorthAmerica?
  }

  if (['SG', 'MY', 'ID'].includes(city.country)) {
    return 'Singapore';
  }

  if (['AF', 'BD', 'PK', 'IN'].includes(city.country)) {
    return 'Karachi';
  }

  if (['BH', 'JO', 'KW', 'OM', 'QA', 'SA', 'SY', 'AE', 'YE'].includes(city.country)) {
    return 'UmmAlQura';
  }

  // No preference (any generally accepted method can be used)
  return null;
}

function isGenerallyAcceptedMethod(methodName) {
  return ['MuslimWorldLeague', 'Egyptian', 'Karachi', 'NorthAmerica'].includes(methodName);
}

export {
  getPrayerTimes,
  methodAndCityMatch,
  getRecommendedMethod,
  isGenerallyAcceptedMethod,
  Madhab,
  HighLatitudeRule,
  CalculationMethod,
};
