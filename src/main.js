import {createApp} from 'vue'
import App from './App.vue'
import './index.css'

const calendarBaseURL = "https://prayer.isword.nl/calendars";

const app = createApp(App);
app.provide('calendarBaseURL', calendarBaseURL);
app.mount('#app');

console.log("Prayer Calendar is an open source project licensed under the MIT License");
console.log("To view the source code, visit: https://gitlab.com/isword/prayer-calendar");
